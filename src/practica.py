# Función que suma dos enteros
def funcion1(num1, num2):
    pass
    return num1 + num2



# Función que multiplica dos números decimales
def funcion2(num1, num2):
    pass
    return num1 * num2

# Función que concatena dos cadenas de texto
def funcion3(str1, str2):
    pass
    return str1 + str2

# Función que verifica si un número entero es par
def funcion4(numero):
    pass
    if numero % 2 == 0:
        return True
    else:
        return False 


# Función que calcula el área de un triángulo dado su base y altura
def funcion5(base, altura):
    pass
    return 0.5 * base * altura
